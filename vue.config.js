const path = require("path")

module.exports = {
    configureWebpack: {
        module: {
            rules: [
                {
                    test: /\.jison\.ya?ml$/,
                    use: [
                        {
                            loader: path.resolve("src/parser/jison-yaml-loader.js")
                        }
                    ],
                    type: "javascript/auto"
                }
            ]
        }
    }
}
