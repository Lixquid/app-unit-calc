declare module "big-rational" {
    export class BigRational {
        /** Performs addition. */
        add(num: BigRational | string | number): BigRational;
        /** Performs subtraction. */
        subtract(num: BigRational | string | number): BigRational;
        /** Performs multiplication. */
        multiply(by: BigRational | string | number): BigRational;
        /** Performs division. */
        divide(by: BigRational | string | number): BigRational;
        /** Returns the negation of a number. */
        negate(): BigRational;
        /**
         * Converts a bigRational to a string in decimal notation, cut off
         * after the number of digits specified in the `digits` argument. The
         * default number of digits is 10.
         * @param digits Number of digits to display. Defaults to `10`.
         */
        toDecimal(digits?: number): string;
    }

    /**
     * Create new `BigRational` from the given input.
     * If `input` is a `string`, it can either be:
     * an integer (`12345`),
     * a fraction (`2/3`),
     * a mixed fraction (`1_1/2`),
     * or a decimal (`54.05446`).
     * @param input The value to create a `BigRational` from.
     */
    export default function(input: string | number | BigRational): BigRational;
}
