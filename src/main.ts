import { createApp } from "vue";
import App from "./App.vue";
import { create } from "./lib/SIDim";
import { parser } from "./parser/parser.jison.yml";
import "./registerServiceWorker";

parser.yy.create = create;

createApp(App).mount("#app");
