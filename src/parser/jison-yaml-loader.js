const {Parser} = require("jison");
const YAML = require("yaml");

module.exports = function (input) {
    return new Parser(YAML.parse(input)).generate();
}
