import { Dim } from "@/lib/Dim";

export const parser: {
    parse: (
        input: string
    ) =>
        | {
              type: "RESULT";
              value: Dim;
          }
        | {
              type: "CONVERT";
              value: Dim;
              target: Dim;
          };
    yy: {
        create: (magnitude: string, unit: string) => Dim;
    };
};
